### order_loader.py
  
  `order_loader.py` parses and normalizes files according to configuration settings stored as a db entry
  
  `python order_loader.py --config historical_orders --rundate 20150619`

####Properties of the loader config:
- utilizes start and stop dates for minimal change to code by keeping track of settings that might change in the future for a given file
- ORM map from *file columns* to *db columns*

####order\_loader.py
- light wrapper that parses args and utilizes `model/orders.py` for file processing and db operations

####orders.py

- load\_config: gets loader config 
- load\_file\_to\_memory: utilizes pandas to read csv files
- normalize: utilizes a source specific class for further *date, string and numeric* operations
- insert\_to\_db: utilizes a data object for db connection
- finalize: close any open connections, TODO: load summary to be added

*use init.sql to first set the tables*
