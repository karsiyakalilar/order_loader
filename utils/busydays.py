import datetime
from pandas.tseries.offsets import BDay

class BusyDays:

  def __init__(self, month):
    self.month = month

  def quarter(self):
    i = self.month % 3
    if i == 0:
      return self.month / 3
    return (self.month / 3) + 1
