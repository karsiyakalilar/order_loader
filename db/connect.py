import psycopg2

class DB:
  def __init__(self, conn=None):
    self.conn = psycopg2.connect("dbname='mert' user='mert'")
    print self.conn
  
  def query_one(self, sql, args, dao=None):
    if dao:
      self.conn = dao
    cur = self.conn.cursor()
    cur.execute(sql, args)
    row = cur.fetchone()
    cur.close()
    return row

  def get_all(self, sql, args, dao=None):
    if dao:
      self.conn = dao
    cur = self.conn.cursor()
    cur.execute(sql, args)
    rows = cur.fetchall()
    cur.close()
    return rows

  def get_dict(self, sql, args, dao=None):
    if dao:
      self.conn = dao

    cur = self.conn.cursor()
    cur.execute(sql, args)
    rows = cur.fetchall()
    cur.close()
    
    cols = [des[0] for des in cur.description]
    # j = [dict(zip(cols,row)) for row in rows if len(rows)>0]
    j = []

    if len(rows) == 0:
      return j    
    else:
      for row in rows:
        j.append(dict(zip(cols,row)))
    return j

  def insert_many(self, sql, args, dao=None):
    cur = self.conn.cursor()
    cur.executemany(sql, args)
    self.conn.commit()
    cur.close()

  def wrap_up(self):
    self.conn.close()
    print self.conn