"""
  Loader wrapper script
  Please README first, this script will accept 
  a csv order file and load into a postgres db
  where tables in ./init.sql is already setup in the db server
  
  sample cmd: python ./order_loader.py --config historical_orders --rundate 20150619 --filename ./assets/20150619_satis_clean.csv
"""

import argparse
from model.orders import Orders

def parse_args():
  parser = argparse.ArgumentParser()
  parser.add_argument("--path")
  parser.add_argument("--filename")
  parser.add_argument("--config", required=True)
  parser.add_argument("--rundate", required=True)
  return parser.parse_args()

if __name__ == "__main__":
  args = parse_args()
  loader = Orders(args)
  loader.run()
  

