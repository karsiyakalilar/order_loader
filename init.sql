-- create main tables
CREATE TABLE IF NOT EXISTS orders (
  id serial primary key,
  client text not null,
  product_category text,
  product text not null,
  internal_order_id integer,
  sale_date date not null,
  total numeric not null check(total>0),
  currency text,
  quantity integer not null,
  month text not null,
  year integer not null,
  quarter integer not null,
);

DROP TABLE IF EXITS order_configs
CREATE TABLE order_configs (
  -- config properties
  id serial,
  start_date date not null,
  stop_date date not null,
  config_name text,x
  filename text,
  filepath text,
  normalizer_name text,
  -- orders related
  client text not null,
  product text not null,
  internal_order_id text,
  sale_date text not null,
  total text not null,
  notes text default '',
  currency text,
  quantity text,
  industry text,
  month text,
  year text,
  quarter text,
  product_category text,
  primary key (id, filename),
  unique (start_date, stop_date, config_name)
);

-- generate a sample entry
INSERT INTO order_configs (start_date, stop_date,
    config_name, filename, filepath, normalizer_name, client,
    product, internal_order_id, sale_date, total,
    currency, quantity, industry, month, year, quarter, product_category)
values ('1/1/1970','12/31/2015','historical_orders',
    'YYYYMMDD_satis_clean.csv','assets','Burcin',
    'firma','urun','siparis_no','tarih','tutar','para_birimi','adet','kullanim_alani','month',
    'year','quarter', 'product_category');
