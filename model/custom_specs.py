import re
from pandas import DataFrame
from pandas import to_datetime
from utils.busydays import BusyDays

class Burcin:

  """
    Normalize values specific to files coming from source "Burcin"
  """

  def __init__(self, data, settings):
    """
      @param data: pandas.DataFrame to be normalized
      @param settings: ORM details (dict)
    """
    self.data = data
    self.settings = settings

  def normalize(self):
    """
    """
    d = self.data

    # - fill data
    d = d.fillna(method="pad")

    # - date operations
    d["year"] = d["tarih"].str.split("/").str.get(2).apply(int)
    d["month"] = d["tarih"].str.split("/").str.get(0).apply(int)
    d["day"] = d["tarih"].str.split("/").str.get(1).apply(int)
    d["quarter"] = d["month"].apply(lambda x: BusyDays(x).quarter())
    d["tarih"] = to_datetime(d["tarih"])

    # - string operations
    d["tutar"] = d["tutar"].apply(lambda x: self.format_to_float(x))
    
    # - products pattern to search in notlar
    d["urun"] = d["notlar"].str.extract('(\w{1,3}-\d{1,3})').fillna("-")

    # - additional columns
    d["product_category"] = d["urun"]
    d["slug"] = d["firma"].str.replace(" ", "_")

    # - rearrange for insert
    req_db_cols = ["product", "product_category", "client", "internal_order_id",
       "sale_date", "total", "currency",
       "quantity", "month", "year", "quarter"]

    # - create a new df that represents db
    tbi = DataFrame(columns=req_db_cols)
    for col in req_db_cols:
      # - use config column mapping
      tbi[col] = d[self.settings[col]]

    return tbi

  def format_to_float(self, x):
    """
      str|int to float
    """
    p = re.compile(",")
    if type(x) == str:
      return float(re.sub(",","",x))
    return float(x)
