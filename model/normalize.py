from custom_specs import Burcin

class BaseNormalizer:
    """
        Dummy normalizer class, just passes the data as is
    """
    def __init__(self, data, settings):
        self.data = data
        self.settings = settings

    def normalize(self):
        return data

class Normalizer:
    """
        Normalizer router class
    """
    def __init__(self, data, settings, normalizer_name):
        self.data = data
        self.settings = settings
        self.normalizer_name = normalizer_name
        self.get_normalizer()

    def get_normalizer(self):
        """
            Normalizer routing
        """
        if self.normalizer_name == "Burcin":
            self.normalizer = Burcin(data, settings)
        else:
            self.normalizer = BaseNormalizer(data, settings)

    def normalize(self):
        """
            Normalizes DataFrame according specific business logic
        """
        return self.normalizer.normalize()
