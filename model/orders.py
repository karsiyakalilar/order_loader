"""
    This class will load the config file 
    and file to be loaded into the memory at initialization
    it will then normalize the file according the ORM map from the config
    and load the normalized content into db

"""
import re
import os
import datetime

import pandas as pd

from db.connect import DB
from .normalize import Normalizer

# - setup logging

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class Orders:
    """
        Order Loader
    """
    def __init__(self, args, debug=False):
        """
            @param args: cmd-line args (dict)
            @param debug: verbose logging (boolean)
        """
        self.args = args
        self.rundate= args.rundate
        self.config = args.config
        self.dbh = DB()
        self.settings = self.load_config(self.config)
        self.data = self.load_file_to_memory()
        self.debug = debug

    def load_config(self, args):
        """
            get db.column to file.column mapping for a given config
            @param args: cmd-line args, we really only need the filename here
            @return: config (dict)
        """
        logger.info("getting configuration for: %s" % (self.config))
        # - query from DB
        today_str = datetime.datetime.today().strftime("%m/%d/%Y")
        sql = """
            select *
            from order_configs
            where config_name = %s
            and %s between start_date and stop_date
        """
        settings = self.dbh.get_dict(sql, (self.config, today_str))[0]
        # - attach settings as attributes
        for field in settings:
            # - skip filename if specified
            if self.args.filename and field == "filename": continue
            setattr(self, field, settings[field])
        return settings

    def load_file_to_memory(self):
        """
            loads file into memory
            @return: raw data file pandas.DataFrame
        """
        fullpath = os.path.join(self.filepath,self.filename)
        fullpath = re.sub("YYYYMMDD", self.rundate, fullpath)
        d = pd.read_csv(fullpath)
        return d

    def normalize(self):
        """
            uses loaded file and maps db columns to the file columns
            stores normalized frame on self.normalized
        """
        # - add config options to understand this
        # - normalizer = Burcin(self.data, self.settings)
        normalizer = Normalizer(self.data, self.settings, self.normalizer_name)
        self.normalized = normalizer.normalize()
        if self.debug:
            logger.debug(self.normalized)

    def insert_to_db(self):
        """
            Inserts self.normalized into the db
        """
        sql = """
            INSERT INTO orders ("product", "product_category", "client", "internal_order_id",
            "sale_date", "total", "currency",
            "quantity", "month", "year", "quarter")
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """

        try:
            self.dbh.insert_many(sql, self.normalized.values.tolist())
        except:
            logger.error("Insert failed")
        
        logger.info("finished inserting entries")

    def finalize(self):
        """
            Closes db connection
        """
        self.dbh.wrap_up()

    def run(self):
        """
            Main execution plan
        """
        self.normalize()
        self.insert_to_db()
        self.finalize()